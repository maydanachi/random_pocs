const namespaceSelector = {
    svg: 'http://www.w3.org/2000/svg',
    path: 'http://www.w3.org/2000/svg'
}

function createElement ({
    attributes = {},
    identifiers = {},
    isNameSpace = false,
    tag = 'div',
    text = '',
    parent
}){
    const { classes, id } = identifiers

    const newElement = isNameSpace
        ? document.createElementNS(namespaceSelector[tag], tag)
        : document.createElement(tag)

    const attributesList = Object.keys(attributes)

    if (attributes && attributesList.length > 0)
        attributesList.forEach(
                key => newElement.setAttribute(key, attributes[key])
            )

    if (identifiers && classes) {
        const listClasses = classes.split(' ')
        listClasses.forEach(className => newElement.classList.add(className))
    }

    if (identifiers && id)
        newElement.id = id

    if (text)
        newElement.innerHTML = text

    if (parent)
        parent.appendChild(newElement)

    return newElement
}

export default createElement
