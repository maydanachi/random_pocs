export function generateDeck (ranking, suits) {

	return suits.reduce((deck, suit) => {
		const suitedRanking = ranking.map(value => {
			return `${value}${suit.toLowerCase()}`
		})
		return [...deck, ...suitedRanking]
	}, [])
}

export function generateHand (deck, maxCards) {

	const hand = []

	for(let i = 0; i < maxCards; i++) {
		
		let index = 0
		let repeated = true
		
		while (repeated) {
	
			index = randomFromRange(0, 51)
			const repeatedIndexes = hand.filter(card => card.index === index)
			repeated = repeatedIndexes.length !== 0
		}
	
		hand.push({
			index: index,
			card: deck[index]
		})
	}

	return hand
}

export function randomFromRange (min, max) {

	return Math.floor(Math.random() * (max - min + 1) + min)
}
