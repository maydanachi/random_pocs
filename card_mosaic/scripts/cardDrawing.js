import createElement from './elementCreation.js'
import {
    adjustment,
    pathClubs,
    pathDiamonds,
    pathHearts,
    pathSpades
} from './vectorPaths.js'

const PATH_SELECTOR = {
    s: { path: pathSpades, name: 'spades' },
    h: { path: pathHearts, name: 'hearts' },
    d: { path: pathDiamonds, name: 'diamonds' },
    c: { path: pathClubs, name: 'clubs' }
}
const SUIT_COUNT = 3
const suitDelimiter = /([shdc])/

function drawCard(cardInfo, parent) {

    const { card } = cardInfo
    const [value, suit] = card.split(suitDelimiter)
    
    const cardElement = createElement({
        identifiers: {
            classes: `card style-A ${PATH_SELECTOR[suit].name}`
        }
    })

    createElement({
        identifiers: {
            classes: 'value'
        },
        text: value,
        parent: cardElement
    })

    for(let count = 0; count < SUIT_COUNT; count++) {

        const suitSVG = getSuitSVG(PATH_SELECTOR[suit], adjustment)
        const cardSuit = createElement({
            identifiers: { classes: 'suit' }
        })
        cardSuit.appendChild(suitSVG)
        cardElement.appendChild(cardSuit)
    }

    parent.appendChild(cardElement)
}

function getSuitSVG (suit, adjustment) {

    const { path, name } = suit

    const suitElement = createElement({
        tag: 'svg',
        isNameSpace: true,
        attributes: { viewBox: adjustment },
        identifiers: { classes: `${name}` }
    })

    createElement({
        tag: 'path',
        isNameSpace: true,
        attributes: { d: path },
        parent: suitElement
    })

    return suitElement
}

export default drawCard
