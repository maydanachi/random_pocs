import {
    generateDeck,
    generateHand
} from './cardDealing.js'

import drawCard from './cardDrawing.js'

const ranking = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
const suits = ['C', 'D', 'H', 'S']
const handSize = 5
const rowSize = 1
const deck = generateDeck(ranking, suits)

Array.from({ length: rowSize }, () => {
    
    const hand = generateHand(deck, handSize)
    const mosaic = document.querySelector('#mosaic')

    hand.forEach(card => drawCard(card, mosaic))
})
