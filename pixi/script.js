import * as pixi from 'pixi.js'

const app = new pixi.Application()
document.body.appendChild(app.view)

pixi.Loader.add()