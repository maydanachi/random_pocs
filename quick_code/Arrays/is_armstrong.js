function isArmstrong(number) {

    const formatted = number.toString().split('')

    const cubeSum = formatted.reduce((sum, elem) => {
            
            return sum + Math.pow(elem, 3)
        }, 0)

    return cubeSum === number
}


const testCases = [ 0, 1, 153, 370, 371, 407, 123, 100, 41 ]

testCases.forEach(number => {
    console.log(number, isArmstrong(number))
})

console.log(testCases)