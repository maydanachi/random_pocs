function getFibonacci(n) {

    let series = [0, 1]
    lastIndex = 1
    count = 2

    while(count < n) {

        const seriesA = series[lastIndex]
        const seriesB = series[lastIndex - 1]
        const newSeries = seriesA + seriesB

        series.push(newSeries)
        lastIndex = series.length - 1
        count++
    }

    return series
}

console.log(getFibonacci(6))
