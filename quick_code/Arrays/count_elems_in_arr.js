function countElemsInArray(array) {

    return array.reduce((result, elem) => {
        
        result[elem] = result[elem]
        ? result[elem] + 1
        : 1
        
        return result
    }, {})
}

const testCase = ['a', 'b', 'b', 'c', 'e', 'a', 't']

console.log(countElemsInArray(testCase))
