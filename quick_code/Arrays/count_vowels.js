function countVowels(word) {
    
    const vowels = { a: 0, e: 0, i: 0, o: 0, u: 0 }

    return word
        .split('')
        .reduce((count, letter) => {

            const lowerCaseLetter = letter.toLowerCase()

            if(count.hasOwnProperty(lowerCaseLetter)) {
                
                count[lowerCaseLetter]++
            }

            return count

        }, vowels)
}

console.log(countVowels('bat'))
console.log(countVowels('ink'))
console.log(countVowels('argentina'))