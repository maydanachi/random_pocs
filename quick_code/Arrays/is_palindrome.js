
function isPalindrome(word, isCaseSensitive = false) {

    const formatted = formatWord(word, isCaseSensitive)

    const middleIndex = Math.floor(word.length / 2)
    const right = formatted.slice(0, middleIndex)
    const left = formatted.slice(middleIndex, formatted.length)

    let isPalindrome = true

    for(let i = 0; i < right.length; i++){

        const leftIndex = left.length - i - 1
        isPalindrome = right[i] === left[leftIndex]

        if(!isPalindrome) {
            break
        }
    }

    return isPalindrome
}

function formatWord(word, isCaseSensitive) {

    return isCaseSensitive
        ? word.split('')
        : word.toLowerCase().split('')
}


const testCases = [
    'redividers',
    'deified',
    'laptop',
    'radar',
    'level',
    'elvis',
    'car',
    'kayak',
    'lead',
    'racecar',
    '1',
    ''
]

testCases.forEach(word => {
    console.log(word, isPalindrome(word))
})
