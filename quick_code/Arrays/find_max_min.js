
function findMax(source) {
    
    return source.reduce((prev, curr) => {
        
        return prev > curr
        ? prev
        : curr
    })
}

function findMin(source) {
    
    return source.reduce((prev, curr) => {
        
        return prev < curr
        ? prev
        : curr
    })
}

const dummyArray = [1, 3, 4, 5, 23, -1, 0]

console.log(findMax(dummyArray))
console.log(findMin(dummyArray))