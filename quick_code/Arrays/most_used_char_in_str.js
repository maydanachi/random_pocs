function getMostUsedChar(word) {

    const totalCount = word
        .split('')
        .reduce((count, char) => {

        count[char] = count[char]
            ? count[char] + 1
            : 1

        return count
    }, {})

    return Object
        .keys(totalCount)
        .reduce((mostUsed, char) => {

        const currentCount = totalCount[char]
        const isGreater = currentCount >= mostUsed.count
        
        return {
            count: isGreater ? currentCount : mostUsed.count,
            char: isGreater ? char : mostUsed.char
        }
    }, { count: 0, char: null })
}

const dummyData = [
    'helicopter',
    'lord',
    'fibonacci'
]

dummyData.forEach(word => {
    console.log(getMostUsedChar(word))
})