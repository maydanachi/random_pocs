const dummyArray = [1, 3, 4, 132, 32, 4, 5, 67, 1, 54, 46, 87, 98, 100]

function getReversedPairs(data) {

    return data.reduceRight((result, n) => {

        const isOdd = Boolean(n % 2)
        if(!isOdd) {
            result.push(n)
        }git 
        return result
    }, [])
}

const toDuplicate = [1, 3, 6, 5]

console.log(duplicate(toDuplicate))

function duplicate(source) {

    return source.reduce((result, elem) => {
        result.push(elem)
        return result
    }, [...source])
}

console.log(mul(4)(3)(2))

function mul(a) {
    return function(b) {
        return function(c) {
            return a * b * c
        }
    }
}

const addSix = createBase(6)

console.log('addSix', addSix(4))
console.log('addSix', addSix(6))

function createBase(base) {
    return function(toAdd) {
        return base + toAdd
    }
}

const myCounter = counter()

myCounter.add()
myCounter.add()

console.log('counter', myCounter.retrieve())


function counter() {
    let _count = 0
    return {
        add: function(toAdd = 1) {
            _count += toAdd 
        }, 
        retrieve: function() {
            return _count
        }
    }
}
