function isAnagram(wordA, wordB) {

    const lengthA = wordA.length
    const lengthB = wordB.length

    if(lengthA !== lengthB) {
        return false
    }

    const sortedCountA = getCharCount(arrayifyWord(wordA))
    const sortedCountB = getCharCount(arrayifyWord(wordB))

    let isAnagram = true

    for (let i = 0; i < sortedCountA.length; i++) {

        const [charA, countA] = sortedCountA[i]
        const [charB, countB] = sortedCountB[i]

        isAnagram = (charA === charB) && (countA === countB)

        if(!isAnagram) {
            break
        }
    }

    return isAnagram
}

function arrayifyWord(word) {

    return word
        .replace(' ','')
        .split('')
}

function getCharCount(word) {
    const count = word.reduce((count, char) => {
        
        count[char] = count[char]
            ? count[char] + 1
            : 1

        return count
    }, {})

    return Object
        .entries(count)
        .sort(sortCharCount)
}

function sortCharCount(countA, countB) {

    const [charA] = countA
    const [charB] = countB

    if(charA > charB) {
        return 1
    }
    else if(charA < charB) {
        return -1
    }
    else {
        return 0
    }

}

const testCases = [
    ['0011', '1110']
    // ['cat', 'act'],
    // ['top', 'pot'],
    // ['cider', 'cried'],
    // ['fired', 'fried'],
    // ['fired', 'fried'],
    // ['gainly', 'lying'],
    // ['whatever', 'anagram'],
    // ['is whatever', 'whatever is']
]

testCases.forEach(elem => {
    const [wordA, wordB] = elem
    console.log(wordA, wordB, isAnagram(wordA, wordB))
})