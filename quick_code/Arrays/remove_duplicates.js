const dummyArray = ['a', 'b', 'b', 'c', 'e', 'a', 't']

function removeDuplicatesFromArray(source) {

    return source.filter((elem, i, arr) => {

        const eval = arr.indexOf(elem) // gets the uindex of the first occurrence
        return eval == i
    })
}

console.log(removeDuplicatesFromArray(dummyArray))