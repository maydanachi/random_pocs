const words = ["yes", "no", "maybe", "always", "sometimes", "never", "if"];
const order = [5,8,2,9,5,6,3,1];

function rearrangeArray (source, order) {

    return order.reduce((result, elem) => {

        const newElem = source[elem]
        if(newElem) {
            result.push(newElem)
        }
        return result
    }, [])
}

console.log(rearrangeArray(words, order))
