function shiftNPlaces(source, shift = 0) {

    const { length } = source
    const adaptedShift = shift % length
    const isInvalid = adaptedShift === 0

    return isInvalid
        ? [...source]
        : source.reduce((shifted, _, i, arr) => {

        const newIndex = i + adaptedShift
        const isOutOfLowerBound = newIndex < 0
        const isOutOfUpperBound = newIndex > (length - 1)

        const adaptedIndex = isOutOfLowerBound
                ? newIndex + length
                : isOutOfUpperBound
                    ? newIndex - length
                    : newIndex

        return `${shifted}${arr[adaptedIndex]}`

    }, '')
}

const word = 'javascript'

console.log(shiftNPlaces(word.split(''), -3))

// function getCharFromCode(charCode) {
    
//     const codeOffset = 65

//     return String.fromCharCode(charCode + codeOffset)
// }

// [...Array(26)].map((_, i) => {

//     console.log(getCharFromCode(i))
// })
