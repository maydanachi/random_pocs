function sortByValue(propName) {
    
    return function (objA, objB) {
        
        const propA = objA[propName]
        const propB = objB[propName]

        if(propA < propB) {
            return -1
        }
        else if (propA > propB) {
            return 1
        }
        else {
            return 0
        }
    }
}

const testCase = [
    { name: 'alan', value: 0, misc: {}},
    { name: 'janice', value: 100, misc: {}},
    { name: 'aaron', value: 5},
    { name: 'matt', value: 10},
    { name: 'dan', value: 1},
    { name: 'jose', value: 12},
    { name: 'petrof', value: 20},
    { name: 'matias', value: 33}
]

console.log(testCase.sort(sortByValue('name')))