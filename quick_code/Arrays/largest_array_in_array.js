function getLargestArray(source = []) {

    return source.reduce((control, elem, i, arr) => {

        const size = elem.length

        if(size > control.size) {

            control.size = size
            control.largest = [...elem]
        }

        return control
    }, {
        largest: source[0],
        size: source[0].length
    })
}


const dummyArray = [
    [1,2,3],
    [1,2],
    [1,2,3,4,5],
    [1],
    [1,4,3,4,4]
]

console.log(getLargestArray(dummyArray))