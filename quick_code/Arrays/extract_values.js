const input = [
    { name: "John", age: 21, city: "New York" },
    { name: "Mike", age: 28, city: "Moscow" },
    { name: "Danny", age: 30, city: "London" },
    { name: "Lisa", age: 26, city: "Paris" },
    { name: "Sophie", age: 19, city: "Berlin" },
  ];

//   const expected = ["John", "Mike", "Danny", "Lisa", "Sophie"]0

function extractValues(source, propName = 'name') {

    return source.map(elem => elem[propName])
}

console.log(extractValues(input))