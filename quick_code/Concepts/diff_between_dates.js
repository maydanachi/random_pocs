function findDayDifference (start, end) {

    const dateA = new Date(start)
    const dateB = new Date(end)

    const diffInMs = dateA.getTime() - dateB.getTime()

    return diffInMs / (1000 * 60 * 60 * 24)
}


console.log(findDayDifference('11/01/2021', '11/03/2021'))