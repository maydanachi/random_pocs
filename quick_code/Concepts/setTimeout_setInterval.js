const timer1 = setTimeout(bye, 4000)

const timer2 = setInterval(repeatAtIntervals, 1000)
console.log('I\'m running')
console.log(timer1)

function bye() {
    console.log('bye')
}

let counter = 0

function repeatAtIntervals() {
    console.log(counter++)
    if(counter > 5) {
        clearInterval(timer2)
    }
}