// ---- Promises

// let p = new Promise((resolve, reject) => {

//     let a = 1 + 1

//     if(a==2) {
//         resolve({message: 'success'})
//     }
//     else {
//         reject({message: 'failed'})
//     }
// })

// p.then(result => {

//     const { message } = result
//     console.log(200, message)
// })
// .catch(result => {
//     const { message } = result
//     console.log(500, message)
// })


// ---- Callback
const userLeft = false
const userHasError = true

function watchTutorialCallback(callback, errorCallback) {

    if(userLeft) {
        errorCallback({
            name: 'User left',
            message: '500'
        })
    } else if (userHasError || userLeft) {
        errorCallback({
            name: 'User has error',
            message: '500'
        })
    } else {
        callback('All ok')
    }
}

const callback = (message) => {

    console.log(message)
}

const errorCallback = (error) => {
    const {name, message} = error
    console.log(name, message)
}

watchTutorialCallback(callback, errorCallback)