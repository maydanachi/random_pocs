// ---- How to check if a given variable is a number

const number = '-a1123'
const isNumber = !isNaN(number)
console.log(isNumber)

// ---- Explain the difference between Object.freeze() vs const
// const applies to 'bindings' or 'variables'
// Object.freeze() applies to values of objects

const frozenObject = { name: 'fleet foxes' }

frozenObject.name = '3rd of may'
Object.freeze(frozenObject)
frozenObject.name = 'crack up'
console.log(frozenObject)

// ---- How to sort a number array
// Best way is to use the sort method

const numbers = [1, 5, 6, 2, 3]
const ascending = (a, b) => a - b
const descending = (a, b) => b - a

const numbersSorted = [...numbers].sort(descending) // sort is not pure 
console.log({numbersSorted, numbers})

const strings = ['dan', 'foxes', 'avocado']
const stringsSorted = [...strings].sort()
console.log({strings, stringsSorted})


// Whats a closure?

const x = 100
const y = 125
function makeAdder(x) {
    return function(y) {
        return x + y
    }
}

console.log(makeAdder(x)(y))

// How does the this word works?

const person = {
    name: 'Daniel',
    sayName: function() {
        console.log(this.name)
    },
    sayArrowName: () => {
        console.log(this.name)
    }
}

this.name = 'Gene'

const otherSayName = person.sayName
otherSayName() // logs 'undefined'

const boundSayName = person.sayName.bind(person) // explicitly telling js to infer 'this' from 'person'
boundSayName()

const otherSayNameArrow = person.sayArrowName
otherSayNameArrow()


// whats 'prototype' in JS?

// it's an object related to all objects in JS
// allows to inherit between two objects