import './App.css';

import MatrixGenerator from 'views/MatrixGenerator'

function App() {

  return (
    <div className="App">
      <MatrixGenerator size={6}/>
    </div>
  );
}

export default App;
