export const coordinates = [
    {value: 21, x: 0, y: 0},
    {value: 13, x: 21, y: 0},
    {value: 8, x: 26, y: 13},
    {value: 5, x: 21, y: 16},
    {value: 3, x: 21, y: 16},
    {value: 2, x: 24, y: 13},
    {value: 1, x: 25, y: 15},
    {value: 1, x: 24, y: 15}
]