import { Fragment, useEffect, useState, useRef } from 'react'
import { coordinates } from 'utils/dummyFibonacci'
import './MatrixGenerator.css'

export default function MatrixGenerator ({
    size = 1
}) {

    const canvasRef = useRef(null)
    const [context, setContext] = useState(null)

    useEffect(() => {
        const canvas = canvasRef.current
        setContext(canvas.getContext('2d'))
    }, [])

    if(context) {
        
        coordinates.map((elem, i) => {

            const { value: size, x, y } = elem

            context.fillStyle = getRandomHexColor()
            context.fillRect(x, y, size, size)
        })

    }

    return <Fragment>
        <canvas ref={canvasRef} />
    </Fragment>
}

function getRandomHexColor() {
    return `#${Math
        .floor(Math.random()*16777215)
        .toString(16)}`
}
