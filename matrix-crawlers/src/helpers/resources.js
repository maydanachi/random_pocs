export function generateMatrix({
    length,
    width,
    minValue,
    maxValue
}) {
    
    length = length || 1
    width = width || 1

    const matrix = []

    for (let row = 0; row < width; row++) {
        
        const newRow = []

        for (let column = 0; column < length; column++) {

            const newCell = generateRandomInteger(minValue, maxValue)
            newRow.push(newCell)
        }

        matrix.push(newRow)
    }

    return matrix
}

export function generateRandomInteger(min, max){

    const floatingNumber = Math.random() * (1 + max - min) + min 
    
    return Math.floor(floatingNumber)
}

export function getBorderList(origin) {
    
    function validateCoordinate(coordinate, maxCoordinate) {
        
        return coordinate >= 0 && coordinate < maxCoordinate
    }

    function getBorderPoint(origin, delta) {

        const {
            x: originX,
            y: originY,
            maxX,
            maxY
        } = origin

        const { deltaX, deltaY } = delta

        const newDeltaX = originX + deltaX
        const newDeltaY = originY + deltaY
        const inXBounds = validateCoordinate(newDeltaX, maxX)
        const inYBounds = validateCoordinate(newDeltaY, maxY)

        return inXBounds && inYBounds
            ? { x: newDeltaX, y: newDeltaY }
            : null
    }

    function getBordersFromDeltaList(deltaList) {
        
        return deltaList.reduce((list, delta) => {
            
            const borderPoint = getBorderPoint(origin, delta)
            
            if (borderPoint) {
                list.push(borderPoint)
            }

            return list
        }, [])
    }

    return getBordersFromDeltaList([
        { deltaX: -1, deltaY: -1 },
        { deltaX: -1, deltaY: 0 },
        { deltaX: -1, deltaY: 1 },
        { deltaX: 0, deltaY: 1 },
        { deltaX: 1, deltaY: 1 },
        { deltaX: 1, deltaY: 0 },
        { deltaX: 1, deltaY: -1 },
        { deltaX: 0, deltaY: -1 }
    ])
}

export const mockMatrix = [
    [2, 3, 1, 3, 3],
    [2, 3, 1, 3, 4],
    [1, 0, 5, 1, 2],
    [1, 4, 5, 2, 1],
    [2, 5, 3, 1, 5]
]