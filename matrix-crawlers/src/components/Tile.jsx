import React from 'react'
import './Tile.css'

export default function Tile({ coordinates, value, onClick, type }) {

    const { x, y } = coordinates
    const TYPE = {
        ACTIVE: 'active',
        BORDER: 'border-active'
    }
    const className = TYPE[type] || ''

    console.log({className})

    return <div
                className={`tile ${className}`}
                onClick={ () => onClick(coordinates) }
            >
        <div className='value'>{value}</div>
        <div className='coordinates'>{`[${x}, ${y}]`}</div>
    </div>
}