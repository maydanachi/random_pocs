import React, { Fragment, useState } from 'react'
import { mockMatrix, getBorderList } from '../helpers/resources.js'
import Tile from './Tile.jsx'
import './MosaicMatrix.css'

export default function MosaicMatrix() {

    const matrix = mockMatrix
    const NO_SELECTION = {
        x: null,
        y: null,
        isActive: false,
        borders: []
    }
    const [selected, setSelected] = useState(NO_SELECTION)
    const max = {
        maxY: matrix[0].length,
        maxX: matrix.length
    }
    
    function onClick(coordinates) {

        setSelected(prev => {
            
            return prev.x === coordinates.x
                && prev.y === coordinates.y
                ? NO_SELECTION
                : {
                    ...coordinates,
                    isActive: true,
                    borders: getBorderList({
                        ...coordinates,
                        ...max
                    })
                }
        })
    }

    function getTiles() {
        
        const borderList = selected.isActive && selected.borders

        console.log(...selected.borders)
        return matrix.map((row, currentX) => {


            return <div
                    key={currentX}
                    className='row'
                >
                {
                    row.map((point, currentY) => {

                        let isBorder = false
                        const isActive = selected.isActive
                            && selected.x === currentX
                            && selected.y === currentY

                        for(let n = 0; n < borderList.length; n++) {

                            const hasMatch = borderList[n]
                                && currentX === borderList[n].x
                                && currentY === borderList[n].y

                            if(hasMatch) {
                                isBorder = true
                                console.log({ hasMatch, isBorder })
                                borderList.splice(n, 1)
                                break
                            }
                        }

                        const type =
                            isActive ? 'ACTIVE' :
                            isBorder ? 'BORDER' :
                            ''
                        console.log({isActive, isBorder, type})

                        return <Tile
                            key = {`${currentY}-${currentX}`}
                            type = { type }
                            onClick = {onClick}
                            value = {point}
                            coordinates = {{
                                x: currentX,
                                y: currentY,
                            }}
                        />
                    })
                }
            </div>
        })
    }

    return <Fragment>
        <div className='container'>
            {getTiles()}
        </div>
    </Fragment>
}